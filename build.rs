// https://doc.rust-lang.org/cargo/reference/build-scripts.html
extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
    // The bindgen::Builder is the main entry point
    // to bindgen, and lets you build up options for
    // the resulting bindings.
    let bindings = bindgen::Builder::default()
        // The input header we would like to generate
        // bindings for.
        .header("wrapper.h")
        .default_enum_style(bindgen::EnumVariation::Rust)
        .whitelist_type(r"esl_.*")
        .whitelist_function(r"esl_.*")
        .whitelist_var(r"ESL_.*")
        // Finish the builder and generate the bindings.
        .generate().unwrap();

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings.write_to_file(out_path.join("bindings.rs")).unwrap();

    // Tell cargo to tell rustc to link the system sofia-sip-ua library
    // shared library.
    println!("cargo:libdir=esl");
    println!("cargo:rustc-link-lib=fs1.8_esl");
}
