#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(dead_code)]

// make this private after 'wrapper' is complete, but leave maybe always??
pub mod ffi {
    include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
}
